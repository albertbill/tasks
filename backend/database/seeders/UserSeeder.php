<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $users = [
            [
                'email_address' => 'abill2323@gmail.com',
                'password' => bcrypt(12354)
            ],
            [
                'email_address' => 'oktdouglas@gmail.com',
                'password' => bcrypt(12354)
            ],
            [
                'email_address' => 'awish@yahoo.com',
                'password' => bcrypt(12354)
            ],
            [
                'email_address' => 'dotachu2526@hotmail.com',
                'password' => bcrypt(12354)
            ],
        ];


        foreach ($users as $user) {
            User::create([$user]);
        }

    }
}