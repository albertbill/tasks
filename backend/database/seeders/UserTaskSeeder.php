<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UserTask;

class UserTaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $user_tasks = [
            [
                'user_id' => 1,
                'task_id' => 1,
                'due_date' => '2023-03-14 22:04:10',
                'start_time' => '2023-03-10 14:00:00',
                'end_time' => '2023-03-10 14:00:00',
                'remark' => 'Well done completed and signed',
                'status_id' => 1,
            ],
            [
                'user_id' => 1,
                'task_id' => 2,
                'due_date' => '2023-03-14 22:04:10',
                'start_time' => '2023-03-10 14:00:00',
                'end_time' => '2023-04-10 13:00:00',
                'remark' => 'Ready for next steps!',
                'status_id' => 1,
            ],
            [
                'user_id' => 2,
                'task_id' => 1,
                'due_date' => '2023-03-14 22:04:10',
                'start_time' => '2023-03-10 14:00:00',
                'end_time' => '2023-03-13 14:00:00',
                'remark' => 'Fullfilled as requested',
                'status_id' => 1,
            ],
            [
                'user_id' => 2,
                'task_id' => 2,
                'due_date' => '2023-03-14 22:04:10',
                'start_time' => '2023-03-10 14:00:00',
                'end_time' => '2023-03-10 14:00:00',
                'remark' => 'Engauging',
                'status_id' => 2,
            ],
            [
                'user_id' => 2,
                'task_id' => 1,
                'due_date' => '2023-03-14 22:04:10',
                'start_time' => '2023-03-10 14:00:00',
                'end_time' => '2023-03-10 14:00:00',
                'remark' => 'To be continued',
                'status_id' => 3,
            ],
            [
                'user_id' => 3,
                'task_id' => 3,
                'due_date' => '2023-05-01 23:59:59',
                'start_time' => '2023-03-01 14:00:00',
                'end_time' => '2023-05-01 14:00:00',
                'remark' => 'On it',
                'status_id' => 2,
            ],
            [
                'user_id' => 4,
                'task_id' => 3,
                'due_date' => '2023-05-01 23:59:59',
                'start_time' => '2023-03-01 14:00:00',
                'end_time' => '2023-05-01 14:00:00',
                'remark' => '',
                'status_id' => 2,
            ],
            [
                'user_id' => 4,
                'task_id' => 1,
                'due_date' => '2023-03-14 22:04:10',
                'start_time' => '2023-03-10 14:00:00',
                'end_time' => '2023-03-10 14:00:00',
                'remark' => '',
                'status_id' => 1,
            ],
            [
                'user_id' => 4,
                'task_id' => 2,
                'due_date' => '2023-03-14 22:04:10',
                'start_time' => '2023-03-10 14:00:00',
                'end_time' => '2023-03-10 14:00:00',
                'remark' => '',
                'status_id' => 2,
            ]
        ];


        foreach ($user_tasks as $u_tasks) {
            UserTask::create([$u_tasks]);
        }

    }
}