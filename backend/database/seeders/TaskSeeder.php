<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Task;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $tasks = [
            [
                'name' => 'Task 1',
                'description' => 'Work on Laravel Backend APIs',
                'due_date' => '2023-03-14 22:04:10',
                'status_id' => 1
            ],
            [
                'name' => 'Task 2',
                'description' => 'Work on Vue JS Frontend',
                'due_date' => '2023-04-11 22:04:10',
                'status_id' => 3
            ],
            [
                'name' => 'Task 3',
                'description' => 'Perform a Code Walk Through',
                'due_date' => '2023-05-10 22:04:10',
                'status_id' => 2
            ],
            [
                'name' => 'Task 4',
                'description' => 'Deploy the App to AWS',
                'due_date' => '2023-05-10 22:05:10',
                'status_id' => 2
            ]
        ];


        foreach ($tasks as $task) {
            Task::create([$task]);
        }
    }
}