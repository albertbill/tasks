<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Status;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $statuses = [
            [
                'name' => 'Complete'
            ],
            [
                'email' => 'Pending'
            ],
            [
                'email' => 'Incomplete'
            ]
        ];


        foreach ($statuses as $status) {
            Status::create([$status]);
        }
    }
}