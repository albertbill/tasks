<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Status;

class StatusController extends Controller
{
    public function index()
    {
        $data = Status::all();

        return response()->json(['data' => $data]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|unique:status,name',
        ]);

        $data = Status::create($request->all());

        $data = [
            'message' => 'Status Updated Successfully',
            'data' => $data
        ];

        return response()->json($data, 201);
    }

    public function show($id)
    {
        try {
            $data = Status::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Status not found'], 404);
        }

        $data = [
            'message' => 'Status Found',
            'data' => $data
        ];

        return response()->json($data, 200);
    }

    public function update(Request $request, $id)
    {


        $request->validate([
            'name' => 'required|string|unique:status,name,' . $id,
        ]);

        try {
            $data = Status::findOrFail($id);
            $data->update($request->all());
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Status not found'], 404);
        }

        $data = [
            'message' => 'Status Updated Successfully',
            'data' => $data
        ];


        return response()->json($data, 200);
    }

    public function archive($id)
    {
        try {
            $data = Status::findOrFail($id);
            $data->delete();
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Status not found'], 404);
        }

        return response()->json([], 204);
    }

    public function destroy($id)
    {


        try {
            $data = Status::withTrashed()->findOrFail($id);
            $data->forceDelete();
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Status not found'], 404);
        }


        $data = [
            'message' => 'Status Deleted Successfully',
            'data' => $data
        ];

        return response()->json($data, 204);
    }
}