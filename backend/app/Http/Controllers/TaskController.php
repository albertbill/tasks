<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\UserTask;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TaskController extends Controller
{
    public function index()
    {
        $tasks = Task::with('users')->get();

        return response()->json(['data' => $tasks]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|unique:tasks,name',
            'description' => 'required|string',
            'due_date' => 'required|date',
            'status_id' => 'required|numeric|exists:status,id'
        ]);

        $task = Task::create($request->all());

        $data = [
            'message' => 'Task Added Successfully',
            'data' => $task
        ];

        return response()->json($data, 201);
    }

    public function assign(Request $request, $id)
    {
        try {

            $request->validate([
                'user_ids' => 'required',
                'user_ids.*' => 'required|numeric|exists:users,id',
                'task_id' => 'required|numeric|exists:tasks,id',
                'due_date' => 'required|date',
                'start_time' => 'nullable|date',
                'end_time' => 'nullable|date',
                'remarks' => 'nullable|string',
                'status_id' => 'required|numeric|exists:status,id',
            ]);


            $user_ids = $request->user_ids;

            foreach ($user_ids as $user_id) {

                //user_id, task_id, due_date, start_time, end_time, remarks, status_id
                $user_task = new UserTask;

                $user_task->user_id = $user_id;
                $user_task->task_id = $id;
                $user_task->due_date = $request->due_date;
                $user_task->start_time = $request->start_time;
                $user_task->end_time = $request->end_time;
                $user_task->remarks = $request->remarks;
                $user_task->status_id = $request->status_id;

                $user_task->save();
            }
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Something went a mess while assigning tasks'], 500);
        }

        return response()->json(['Message' => 'Tasks Assigned Successfully!']);
    }




    public function assignUpdate(Request $request, $id)
    {
        $request->validate([
            'user_ids' => 'required',
            'user_ids.*' => 'required|numeric|exists:users,id',
            'task_id' => 'required|numeric|exists:tasks,id',
            'due_date' => 'required|date',
            'start_time' => 'nullable|date',
            'end_time' => 'nullable|date',
            'remarks' => 'nullable|string',
            'status_id' => 'required|numeric|exists:status,id',
        ]);

        $user_ids = $request->user_ids;

        $task_users = UserTask::where('task_id', $id)->get();

        foreach ($task_users as $task_user) {

            if (!in_array($task_user->user_id, $user_ids)) {
                $task_user->forceDelete();
            }
        }

        foreach ($user_ids as $user_id) {

            $existingUserTask = UserTask::where('task_id', $id)->where('user_id', $user_id)->first();

            if (!$existingUserTask) {
                $user_task = new UserTask;

                $user_task->user_id = $user_id;
                $user_task->task_id = $id;
                $user_task->due_date = $request->due_date;
                $user_task->start_time = $request->start_time;
                $user_task->end_time = $request->end_time;
                $user_task->remarks = $request->remarks;
                $user_task->status_id = $request->status_id;

                $user_task->save();
            }
        }

        return response()->json(['Message' => 'Task Updated Successfully!']);
    }

    public function show($id)
    {
        try {
            $task = Task::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Task not found'], 404);
        }

        return response()->json(['data' => $task]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|unique:tasks,name,' . $id,
            'description' => 'required|string',
            'due_date' => 'required|date',
            'status_id' => 'required|numeric|exists:status,id'
        ]);

        try {
            $task = Task::findOrFail($id);
            $task->update($request->all());
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Task not found'], 404);
        }

        return response()->json(['data' => $task]);
    }

    public function archive($id)
    {
        try {
            $task = Task::findOrFail($id);
            $task->delete();
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Task not found'], 404);
        }

        return response()->json([], 204);
    }

    public function destroy($id)
    {
        try {
            $task = Task::withTrashed()->findOrFail($id);
            $task->forceDelete();
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'Task not found'], 404);
        }

        return response()->json([], 204);
    }
}