<?php

namespace App\Http\Controllers;

use App\Models\UserTask;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserController extends Controller
{
    public function index()
    {
        $data = User::with('tasks')->get();

        return response()->json(['data' => $data]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'email_address' => 'required|email|unique:users,email_address',
        ]);
        $password = bcrypt(12345);
        $data = User::create([
            'email_address' => $request->email_address,
            'password' => $password
        ]);

        return response()->json(['data' => $data], 201);
    }

    public function show($id)
    {
        try {
            $data = User::findOrFail($id);
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'User not found'], 404);
        }

        return response()->json(['data' => $data], 200);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'email_address' => 'required|email|unique:users,email_address,' . $id
        ]);

        try {
            $data = User::findOrFail($id);
            $data->update($request->all());
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'User not found'], 404);
        }

        return response()->json(['data' => $data]);
    }

    public function archive($id)
    {
        try {
            $data = User::findOrFail($id);
            $data->delete();
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'User not found'], 404);
        }

        return response()->json([], 204);
    }

    public function restore($id)
    {
        try {
            $data = User::onlyTrashed()->findOrFail($id);
            $data->restore();
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'User not found'], 404);
        }

        return response()->json($data);
    }


    public function destroy($id)
    {
        try {
            $data = User::withTrashed()->findOrFail($id);
            $data->forceDelete();
        } catch (ModelNotFoundException $e) {
            return response()->json(['error' => 'User not found'], 404);
        }

        return response()->json([], 204);
    }
}