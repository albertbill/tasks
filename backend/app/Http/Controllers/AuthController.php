<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request)
    {

        $fields = $request->validate([
            'email_address' => 'required|email|exists:users,email_address',
            'password' => 'required|string'
        ]);

        $user = User::where('email_address', $fields['email_address'])->first();

        if (!$user || !Hash::check($fields['password'], $user->password)) {
            return response([
                'message' => 'Invalid username or password!'
            ], 401);
        }

        $token = $user->createToken('myAppToken')->plainTextToken;
        $response = [
            'user' => $user,
            'token' => $token
        ];
        return response($response, 201);
    }


    public function logout(User $user)
    {
        $user->tokens()->delete();
        return response([
            'message' => 'Logged Out!'
        ], 200);
    }
}