import axios from 'axios';

const apiClient = axios.create({
    baseURL: import.meta.env.VITE_VUE_APP_URL,
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
    }
});

export default {
    //auth
    LoginUser(user) {
        return apiClient.post('/login', user);
    },
    LogoutUser(user) {
        return apiClient.post('/logout', user);
    },


    //users endpoints
    getAllUsers() {
        return apiClient.get('/users');
    },
    createUser(user) {
        return apiClient.post('/users', user);
    },
    updateUser(user) {
        return apiClient.put('/users/' + user.id, user);
    },
    archiveUser(user) {
        return apiClient.delete('/users/' + user.id + '/archive', user);
    },
    restoreUser(user) {
        return apiClient.put('/users/' + user.id + '/restore', user);
    },
    destroyUser(user) {
        return apiClient.delete('/users/' + user.id + '/delete', user);
    },


    //tasks endpoints
    getAllTasks() {
        return apiClient.get('/tasks');
    },
    createTasks(task) {
        return apiClient.post('/tasks', task);
    },
    updateTasks(task) {
        return apiClient.put('/tasks/' + task.id, task);
    },
    assign(task) {
        return apiClient.put('/tasks/' + task.id + '/assign', task);
    },
    archiveTasks(task) {
        return apiClient.delete('/tasks/' + task.id + '/archive', task);
    },
    restoreTasks(task) {
        return apiClient.put('/tasks/' + task.id + '/restore', task);
    },
    destroyTasks(task) {
        return apiClient.delete('/tasks/' + task.id + '/delete', task);
    },

};