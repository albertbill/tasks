import axios from 'axios';

const apiClient = axios.create({
    baseURL: import.meta.env.VITE_VUE_APP_URL,
    headers: {
        'Content-Type': 'application/json'
    }

});

export default {
    LoginUser(user) {
        return apiClient.post('/login', user);
    },
    LogOutUser(user) {
        return apiClient.post('/logout');
    }
}
