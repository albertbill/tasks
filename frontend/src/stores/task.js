import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useTaskStore = defineStore('task', () => {
  let isLoggedIn = ref(false);
  let user = ref({});
  const doubleCount = computed(() => count.value * 2)
  function increment() {
    count.value++
  }

  function setIsLoggedIn(value) {
    isLoggedIn.value = value;
  }

  return { isLoggedIn, doubleCount, increment , setIsLoggedIn }

})





